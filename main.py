from flask import Flask, request, render_template, redirect

app = Flask(__name__)

@app.route('/')
def home():
    return redirect('/about', 302)

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        return render_template('about.html')
    else:
        return render_template('login.html')

if __name__ == '__main__':
    app.run(debug=True)